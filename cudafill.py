import numpy as np
import _cuda_fill


def histogram(data, edges, weights, flow=False):
    # check no duplicates
    assert np.unique(edges).size == edges.size
    even_binning = np.all(np.diff(edges) == np.diff(edges)[0])
    sumw, sumw2 = _cuda_fill.histogram(
        data=data,
        edges=edges,
        weights=weights,
        variable_binning=not even_binning,
    )
    if flow:
        return sumw, sumw2
    else:
        return sumw[1:-1], sumw2[1:-1]
