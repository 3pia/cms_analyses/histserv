#!/usr/bin/env bash

action() {
    export CUDA_VISIBLE_DEVICES=0
    if [ ! -d build ]; then
        mkdir build;
    fi
    cd build
    cmake ..
    make
    export PYTHONPATH=$PWD:$PYTHONPATH
    cd ..
}
action "$@"
