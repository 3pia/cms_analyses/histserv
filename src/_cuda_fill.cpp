#include <cuda_runtime.h>
#include <iostream>
#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <tuple>

namespace py = pybind11;

void fill(float* data_gpu_ptr, float* edges_gpu_ptr, float* weights_gpu_ptr, int N, int NEDGES, float* sumw_gpu_ptr, float* sumw2_gpu_ptr, bool variable_binning);

std::tuple<py::array_t<float>, py::array_t<float>> histogram(py::array_t<float> data, py::array_t<float> edges, py::array_t<float> weights, bool variable_binning)
{
    auto bdata = data.request();
    auto bedges = edges.request();
    auto bweights = weights.request();

    // check that all arrays are one-dimensional
    if (bdata.ndim != 1 || bedges.ndim != 1 || bweights.ndim != 1)
    {
        std::stringstream strstr;
        strstr << "Arrays must be one-dimensional, got:" << std::endl;
        strstr << "data.ndim: " << bdata.ndim << std::endl;
        strstr << "edges.ndim: " << bedges.ndim << std::endl;
        strstr << "weights.ndim: " << bweights.ndim << std::endl;
        throw std::runtime_error(strstr.str());
    }
    // check that data and weights have same size
    if (bdata.size != bweights.size)
    {
        std::stringstream strstr;
        strstr << "Data and weights must have same size, got:" << std::endl;
        strstr << "data.size: " << bdata.size << std::endl;
        strstr << "weights.size: " << bweights.size << std::endl;
        throw std::runtime_error(strstr.str());
    }

    // now allocate memory on GPU for arrays
    // data
    int data_size = bdata.shape[0];
    int data_size_bytes = data_size * sizeof(float);
    float* data_gpu_ptr;
    // allocate memory
    cudaMalloc(&data_gpu_ptr, data_size_bytes);
    float* data_ptr = reinterpret_cast<float*>(bdata.ptr);
    // copy to GPU
    cudaMemcpy(data_gpu_ptr, data_ptr, data_size_bytes, cudaMemcpyHostToDevice);

    // edges
    int edges_size = bedges.shape[0];
    int* edges_size_ptr;
    int edges_size_bytes = edges_size * sizeof(float);
    float* edges_gpu_ptr;
    // allocate memory
    cudaMalloc(&edges_gpu_ptr, edges_size_bytes);
    cudaMalloc(&edges_size_ptr, edges_size * sizeof(int));
    float* edges_ptr = reinterpret_cast<float*>(bedges.ptr);
    // copy to GPU
    cudaMemcpy(edges_gpu_ptr, edges_ptr, edges_size_bytes, cudaMemcpyHostToDevice);

    // weights
    int weights_size = bweights.shape[0];
    int weights_size_bytes = weights_size * sizeof(float);
    float* weights_gpu_ptr;
    // allocate memory
    cudaMalloc(&weights_gpu_ptr, weights_size_bytes);
    float* weights_ptr = reinterpret_cast<float*>(bweights.ptr);
    // copy to GPU
    cudaMemcpy(weights_gpu_ptr, weights_ptr, weights_size_bytes, cudaMemcpyHostToDevice);

    // output array
    // sumw
    auto sumw = py::array_t<float>(bedges.size + 1);
    py::buffer_info bsumw = sumw.request();
    int sumw_size = bsumw.shape[0];
    int sumw_size_bytes = sumw_size * sizeof(float);
    float* sumw_gpu_ptr;
    // allocate memory
    cudaMalloc(&sumw_gpu_ptr, sumw_size_bytes);
    float* sumw_ptr = reinterpret_cast<float*>(bsumw.ptr);
    for (int i = 0; i < bedges.size + 1; i++)
    {
        sumw_ptr[i] = 0;
    }
    // copy to GPU
    cudaMemcpy(sumw_gpu_ptr, sumw_ptr, sumw_size_bytes, cudaMemcpyHostToDevice);
    // sumw2
    auto sumw2 = py::array_t<float>(bedges.size + 1);
    py::buffer_info bsumw2 = sumw2.request();
    int sumw2_size = bsumw2.shape[0];
    int sumw2_size_bytes = sumw2_size * sizeof(float);
    float* sumw2_gpu_ptr;
    // allocate memory
    cudaMalloc(&sumw2_gpu_ptr, sumw2_size_bytes);
    float* sumw2_ptr = reinterpret_cast<float*>(bsumw2.ptr);
    for (int i = 0; i < bedges.size + 1; i++)
    {
        sumw2_ptr[i] = 0;
    }
    // copy to GPU
    cudaMemcpy(sumw2_gpu_ptr, sumw2_ptr, sumw2_size_bytes, cudaMemcpyHostToDevice);

    // call kernel launcher
    fill(data_gpu_ptr, edges_gpu_ptr, weights_gpu_ptr, data_size, edges_size, sumw_gpu_ptr, sumw2_gpu_ptr, variable_binning);

    // copy back sumw from GPU to host
    cudaMemcpy(sumw_ptr, sumw_gpu_ptr, sumw_size_bytes, cudaMemcpyDeviceToHost);
    cudaMemcpy(sumw2_ptr, sumw2_gpu_ptr, sumw2_size_bytes, cudaMemcpyDeviceToHost);

    cudaFree(data_gpu_ptr);
    cudaFree(edges_gpu_ptr);
    cudaFree(weights_gpu_ptr);
    cudaFree(sumw_gpu_ptr);
    cudaFree(sumw2_gpu_ptr);
    return std::make_tuple(sumw, sumw2);
}

PYBIND11_MODULE(_cuda_fill, m)
{
    m.def("histogram", histogram, "A CUDA-accelerated histogramming function", py::arg("data"), py::arg("edges"), py::arg("weights"), py::arg("variable_binning"));
}