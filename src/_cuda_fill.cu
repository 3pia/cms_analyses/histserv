#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <math.h>
#include <numeric>
#include <vector>

__device__ int bitshiftsearch(float* edges, int count, float val)
{
    int idx, step;
    for (step = 1; step < count; step <<= 1)
        ;
    for (idx = 0; step; step >>= 1)
        if (idx + step < count && edges[idx + step] <= val)
            idx += step;
    return idx;
}

__device__ int get_idx(float* edges, int count, float val, bool variable_binning)
{
    int idx;
    if (variable_binning == true)
    {
        // variable binning
        idx = bitshiftsearch(edges, count, val);
    }
    else
    {
        // even binning
        idx = (val - edges[0]) / (edges[1] - edges[0]);
    }
    return idx;
}

// GPU kernel for computing a histogram
__global__ void kernel(float* data_gpu_ptr, float* edges_gpu_ptr, float* weights_gpu_ptr, int N, int NBINS, float* sumw_gpu_ptr, float* sumw2_gpu_ptr, bool variable_binning)
{
    // Calculate global thread ID
    int tid = blockIdx.x * blockDim.x + threadIdx.x;

    // // Allocate a local histogram for each TB
    // extern __shared__ float s_out[];

    // // Initalize the shared memory to 0
    // if (threadIdx.x < NBINS)
    // {
    //     s_out[threadIdx.x] = 0.0f;
    //     s_out[NBINS + threadIdx.x] = 0.0f;
    // }

    // // Wait for shared memory writes to complete
    // __syncthreads();

    // Calculate the bin positions locally
    for (int i = tid; i < N; i += (gridDim.x * blockDim.x))
    {
        int idx;
        // underflow
        if (data_gpu_ptr[i] < edges_gpu_ptr[0])
        {
            idx = 0;
        }
        // overflow
        else if (data_gpu_ptr[i] > edges_gpu_ptr[NBINS - 2])
        {
            idx = NBINS - 1;
        }
        // rest
        else
        {
            idx = get_idx(edges_gpu_ptr, NBINS - 1, data_gpu_ptr[i], variable_binning) + 1;
        }
        // atomicAdd(&s_out[idx], weights_gpu_ptr[i]);
        // atomicAdd(&s_out[NBINS + idx], weights_gpu_ptr[i] * weights_gpu_ptr[i]);
        atomicAdd(&sumw_gpu_ptr[idx], weights_gpu_ptr[i]);
        atomicAdd(&sumw2_gpu_ptr[idx], weights_gpu_ptr[i] * weights_gpu_ptr[i]);
    }

    // // Wait for shared memory writes to complete
    // __syncthreads();

    // // Combine the partial results (sumw, sumw2)
    // if (threadIdx.x < NBINS)
    // {
    //     atomicAdd(&sumw_gpu_ptr[threadIdx.x], s_out[threadIdx.x]);
    //     atomicAdd(&sumw2_gpu_ptr[threadIdx.x], s_out[NBINS + threadIdx.x]);
    // }
}

void fill(float* data_gpu_ptr, float* edges_gpu_ptr, float* weights_gpu_ptr, int N, int NEDGES, float* sumw_gpu_ptr, float* sumw2_gpu_ptr, bool variable_binning)
{
    // Number of bins (+2 for under-/overflow)
    int NBINS = NEDGES + 1;

    // Number of threads per threadblock
    int THREADS = 512;
    dim3 threadPerBlock(THREADS, 1, 1);
    // Calculate the number of threadblocks
    dim3 blockPerGrid(N / THREADS, 1, 1);

    // Launch the kernel
    //kernel<<<blockPerGrid, threadPerBlock, 2 * NBINS * sizeof(float)>>>(data_gpu_ptr, edges_gpu_ptr, weights_gpu_ptr, N, NBINS, sumw_gpu_ptr, sumw2_gpu_ptr, variable_binning);
    kernel<<<blockPerGrid, threadPerBlock>>>(data_gpu_ptr, edges_gpu_ptr, weights_gpu_ptr, N, NBINS, sumw_gpu_ptr, sumw2_gpu_ptr, variable_binning);
}