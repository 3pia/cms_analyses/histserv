import cudafill
import numpy as np
from timeit import default_timer as timer


sizes = [1000, 10000, 100000, 1000000]
bins = [10, 50, 100, 400]


for s in sizes:
    data = np.random.rand(int(s)).astype("float32")
    weights = 0.5 * np.random.rand(int(s)).astype("float32")
    print(f"SIZE: {s}")
    for b in bins:
        edges = np.linspace(0, 1, b + 1).astype("float32")
        cudastart = timer()
        sumwcuda, sumw2cuda = cudafill.histogram(data=data, edges=edges, weights=weights, flow=True)
        cudaend = timer()
        npstart = timer()
        sumwnp, _ = np.histogram(
            data,
            bins=edges,
            weights=weights,
        )
        npend = timer()
        # check that it is the same
        assert sumwcuda[0] == 0
        assert sumwcuda[-1] == 0
        if not np.allclose(sumwcuda[1:-1], sumwnp):
            from IPython import embed

            embed()
        print(
            f"    * {b} bins: Numpy={npend-npstart}, CUDA={cudaend-cudastart}, Speedup(Numpy/CUDA)={(npend-npstart)/(cudaend-cudastart):.2f}x"
        )
    print("")

from IPython import embed

embed()


"""
SIZE: 1000
    * 10 bins: Numpy=0.00028410274535417557, CUDA=0.10797374602407217, Speedup(Numpy/CUDA)=0.00x
    * 50 bins: Numpy=0.0001848321408033371, CUDA=0.00024550221860408783, Speedup(Numpy/CUDA)=0.75x
    * 100 bins: Numpy=0.00017270119860768318, CUDA=0.00022569205611944199, Speedup(Numpy/CUDA)=0.77x
    * 400 bins: Numpy=0.0001893206499516964, CUDA=0.00022047199308872223, Speedup(Numpy/CUDA)=0.86x

SIZE: 10000
    * 10 bins: Numpy=0.001239030621945858, CUDA=0.0002689617685973644, Speedup(Numpy/CUDA)=4.61x
    * 50 bins: Numpy=0.0011074589565396309, CUDA=0.000286842230707407, Speedup(Numpy/CUDA)=3.86x
    * 100 bins: Numpy=0.0011167293414473534, CUDA=0.00024411221966147423, Speedup(Numpy/CUDA)=4.57x
    * 400 bins: Numpy=0.001108759082853794, CUDA=0.00029555289074778557, Speedup(Numpy/CUDA)=3.75x

SIZE: 100000
    * 10 bins: Numpy=0.01283912779763341, CUDA=0.0006597652100026608, Speedup(Numpy/CUDA)=19.46x
    * 50 bins: Numpy=0.012082321103662252, CUDA=0.0005462649278342724, Speedup(Numpy/CUDA)=22.12x
    * 100 bins: Numpy=0.011864010244607925, CUDA=0.0005729449912905693, Speedup(Numpy/CUDA)=20.71x
    * 400 bins: Numpy=0.011901370715349913, CUDA=0.00047476403415203094, Speedup(Numpy/CUDA)=25.07x

SIZE: 1000000
    * 10 bins: Numpy=0.12187658157199621, CUDA=0.004082075320184231, Speedup(Numpy/CUDA)=29.86x
    * 50 bins: Numpy=0.12894196202978492, CUDA=0.004662678577005863, Speedup(Numpy/CUDA)=27.65x
    * 100 bins: Numpy=0.12179386196658015, CUDA=0.003066325094550848, Speedup(Numpy/CUDA)=39.72x
    * 400 bins: Numpy=0.12347594602033496, CUDA=0.0033367876894772053, Speedup(Numpy/CUDA)=37.00x
"""